<?php

namespace Tests\Unit;

use App\Models\Services\Nhtsa;
use Tests\TestCase;
use Mockery;
use GuzzleHttp;
use GuzzleHttp\Psr7\Response;

class NhtsaTest extends TestCase
{
    /**
     * @test
     */
    public function call_to_getVehicleModels_does_an_external_call_to_public_api_and_returns_its_data_if_call_does_not_fail()
    {
        $url = 'https://one.nhtsa.gov/webapi/api/SafetyRatings/modelyear/fake-model/make/fake-manufacturer/model/fake-model?format=json';

        $response = new Response(200, ['X-Foo' => 'Bar'], json_encode(['fake-response']));
        $guzzle_mock = Mockery::mock(GuzzleHttp\Client::class);
        $guzzle_mock
            ->shouldReceive('get')
            ->with($url)
            ->once()
            ->andReturn($response);

        $nhtsa = new Nhtsa($guzzle_mock);
        $response = $nhtsa->getVehicleModels('fake-model', 'fake-manufacturer', 'fake-model');

        $this->assertEquals(['fake-response'], $response);
    }

    /**
     * @test
     */
    public function call_to_getVehicleRating_does_an_external_call_to_public_api_and_returns_its_data_if_call_does_not_fail()
    {
        $url = 'https://one.nhtsa.gov/webapi/api/SafetyRatings/VehicleId/fake-vehicle-id?format=json';

        $response = new Response(200, ['X-Foo' => 'Bar'], json_encode(['fake-response']));
        $guzzle_mock = Mockery::mock(GuzzleHttp\Client::class);
        $guzzle_mock
            ->shouldReceive('get')
            ->with($url)
            ->once()
            ->andReturn($response);

        $nhtsa = new Nhtsa($guzzle_mock);
        $response = $nhtsa->getVehicleRating('fake-vehicle-id');

        $this->assertEquals(['fake-response'], $response);
    }

    /**
     * @test
     */
    public function call_to_getVehicleModelsWithRating_makes_a_call_to_getVehicleModels_and_getVehicleRating_and_add_CrashRating_attribute()
    {
        $nhtsa_mock = Mockery::mock(Nhtsa::class)->makePartial();
        $nhtsa_mock
            ->shouldReceive('getVehicleModels')
            ->once()
            ->andReturn([
                'Results' => [
                    ['VehicleId' => 123]
                ]
            ])
            ->shouldReceive('getVehicleRating')
            ->once()
            ->andReturn([
                'Results' => [
                    ['OverallRating' => 'rating']
                ]
            ]);

        $response = $nhtsa_mock->getVehicleModelsWithRating(132, 'fake-manu', 'fake-model');

        $this->assertEquals(['Results' => [
            [
                'VehicleId' => 123,
                'CrashRating' => 'rating',
            ]
        ]], $response);
    }
}
