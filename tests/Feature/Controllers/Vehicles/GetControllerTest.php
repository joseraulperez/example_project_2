<?php

namespace Tests\Feature;

use App\Models\Services\Nhtsa;
use Mockery;
use Tests\TestCase;

class GetControllerTest extends TestCase
{
    /**
     * @test
     */
    public function if_year_parameter_is_not_numeric_string_returns_empty_json()
    {
        $response = $this->get('/vehicles/not-a-number/fake-manufacturer/fake-model');

        $response
            ->assertStatus(200)
            ->assertExactJson([
                'Count' => 0,
                'Results' => [],
            ]);
    }

    /**
     * @test
     * @dataProvider dataProvider
     *
     * @param $get_parameters
     * @param $method
     * @param $external_api_response
     * @param $api_response
     */
    public function endpoint_call_with_correct_parameters($get_parameters, $method, $external_api_response, $api_response)
    {
        $nhtsa_mock = Mockery::mock(Nhtsa::class);
        $nhtsa_mock
            ->shouldReceive($method)
            ->with('12345', 'fake-manufacturer', 'fake-model')
            ->once()
            ->andReturn($external_api_response);

        $this->app->instance(Nhtsa::class, $nhtsa_mock);

        $response = $this->get('/vehicles/12345/fake-manufacturer/fake-model'.$get_parameters);

        $response
            ->assertStatus(200)
            ->assertExactJson($api_response);
    }

    public function dataProvider()
    {
        return [
            'if external service does not returns data, it returns json with Results:[] Count:0' => [
                '',
                'getVehicleModels',
                [
                    'Count' => 0,
                    'Message' => 'Fake Message',
                    'Results' => [],
                ],
                [
                    'Count' => 0,
                    'Results' => [],
                ]
            ],
            'if external service returns data, it changes attributes' => [
                '',
                'getVehicleModels',
                [
                    'Count' => 1,
                    'Message' => 'Fake Message',
                    'Results' => [
                        [
                            'VehicleDescription' => 'Fake description',
                            'VehicleId' => 123456,
                        ]
                    ],
                ],
                [
                    'Count' => 1,
                    'Results' => [
                        [
                            'Description' => 'Fake description',
                            'VehicleId' => 123456,
                        ],
                    ],
                ]
            ],
            'if "withRating" parameter is present and is "true", add "CrashRating" attribute' => [
                '?withRating=true',
                'getVehicleModelsWithRating',
                [
                    'Count' => 1,
                    'Message' => 'Fake Message',
                    'Results' => [
                        [
                            'VehicleDescription' => 'Fake description',
                            'VehicleId' => 123456,
                            'CrashRating' => 'Fake rating',
                        ]
                    ],
                ],
                [
                    'Count' => 1,
                    'Results' => [
                        [
                            'Description' => 'Fake description',
                            'VehicleId' => 123456,
                            'CrashRating' => 'Fake rating',
                        ],
                    ],
                ]
            ],
            'if "withRating" parameter is present but is not "true", do not add "CrashRating" attribute' => [
                '?withRating=im_sad_because_im_not_true',
                'getVehicleModels',
                [
                    'Count' => 1,
                    'Message' => 'Fake Message',
                    'Results' => [
                        [
                            'VehicleDescription' => 'Fake description',
                            'VehicleId' => 123456,
                        ]
                    ],
                ],
                [
                    'Count' => 1,
                    'Results' => [
                        [
                            'Description' => 'Fake description',
                            'VehicleId' => 123456,
                        ],
                    ],
                ]
            ],
        ];
    }
}
