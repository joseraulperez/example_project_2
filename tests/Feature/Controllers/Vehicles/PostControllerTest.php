<?php

namespace Tests\Feature;

use App\Models\Services\Nhtsa;
use Mockery;
use Tests\TestCase;

class PostControllerTest extends TestCase
{
    /**
     * @test
     * @dataProvider dataProvider
     *
     * @param $post_parameters
     * @param $external_api_response
     * @param $api_response
     */
    public function endpoint_call_with_all_parameters($post_parameters, $external_api_response, $api_response)
    {
        $nhtsa_mock = Mockery::mock(Nhtsa::class);
        $nhtsa_mock
            ->shouldReceive('getVehicleModels')
            ->with($post_parameters['modelYear'], $post_parameters['manufacturer'], $post_parameters['model'])
            ->once()
            ->andReturn($external_api_response);

        $this->app->instance(Nhtsa::class, $nhtsa_mock);

        $response = $this->post('/vehicles', $post_parameters);

        $response
            ->assertStatus(200)
            ->assertExactJson($api_response);
    }

    public function dataProvider()
    {
        return [
            'if external service does not returns data, it returns json with Results:[] Count:0' => [
                [
                   'modelYear' => 1234,
                   'manufacturer' => 'fake manufacturer',
                   'model' => 'fake model',
                ],
                [
                    'Count' => 0,
                    'Message' => 'Fake Message',
                    'Results' => [],
                ],
                [
                    'Count' => 0,
                    'Results' => [],
                ]
            ],
            'if external service returns data, it changes attributes' => [
                [
                    'modelYear' => 1234,
                    'manufacturer' => 'fake manufacturer',
                    'model' => 'fake model',
                ],
                [
                    'Count' => 1,
                    'Message' => 'Fake Message',
                    'Results' => [
                        [
                            'VehicleDescription' => 'Fake description',
                            'VehicleId' => 123456,
                        ]
                    ],
                ],
                [
                    'Count' => 1,
                    'Results' => [
                        [
                            'Description' => 'Fake description',
                            'VehicleId' => 123456,
                        ],
                    ],
                ]
            ],
        ];
    }

    /**
     * @test
     * @dataProvider dataProviderForEndpointMissingParameters
     *
     * @param $post_parameters
     * @param $api_response
     */
    public function endpoint_call_missing_or_wrong_parameters($post_parameters, $api_response)
    {
        $response = $this->post('/vehicles', $post_parameters);

        $response
            ->assertStatus(200)
            ->assertExactJson($api_response);
    }

    public function dataProviderForEndpointMissingParameters()
    {
        return [
            'if "modelYear" is missing, it returns json with Results:[] Count:0' => [
                [
                    'modelYear' => null,
                    'manufacturer' => 'fake manufacturer',
                    'model' => 'fake model',
                ],
                [
                    'Count' => 0,
                    'Results' => [],
                ]
            ],
            'if "manufacturer" is missing, it returns json with Results:[] Count:0' => [
                [
                    'modelYear' => 1324,
                    'manufacturer' => null,
                    'model' => 'fake year',
                ],
                [
                    'Count' => 0,
                    'Results' => [],
                ]
            ],
            'if "model" is missing, it returns json with Results:[] Count:0' => [
                [
                    'modelYear' => 1324,
                    'manufacturer' => 'fake manufacturer',
                    'model' => null,
                ],
                [
                    'Count' => 0,
                    'Results' => [],
                ]
            ],
            'if "modelYear" is not a number, it returns json with Results:[] Count:0' => [
                [
                    'modelYear' => 'not a number',
                    'manufacturer' => 'fake manufacturer',
                    'model' => 'fake model',
                ],
                [
                    'Count' => 0,
                    'Results' => [],
                ]
            ],
        ];
    }
}
