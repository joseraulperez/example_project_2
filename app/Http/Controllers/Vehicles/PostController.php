<?php

namespace App\Http\Controllers\Vehicles;

use App\Http\Controllers\Controller;
use App\Http\Traits\PrepareResponse;
use App\Models\Services\Nhtsa;
use Validator;

class PostController extends Controller
{
    use PrepareResponse;

    /**
     * POST endpoint for /vehicles
     *
     * @param  Nhtsa $nhtsa
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Nhtsa $nhtsa)
    {
        $parameters = request()->all();

        $validator = Validator::make($parameters, [
            'modelYear' => 'required|numeric',
            'manufacturer' => 'required',
            'model' => 'required',
        ]);

        if ($validator->fails()) {
            return response()
                ->json($this->prepareEmptyResponse());
        }

        $raw_response = $nhtsa->getVehicleModels($parameters['modelYear'], $parameters['manufacturer'], $parameters['model']);

        return response()
            ->json($this->prepareResponse($raw_response));
    }
}
