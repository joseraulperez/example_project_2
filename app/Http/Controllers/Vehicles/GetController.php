<?php

namespace App\Http\Controllers\Vehicles;

use App\Http\Controllers\Controller;
use App\Models\Services\Nhtsa;
use App\Http\Traits\PrepareResponse;

class GetController extends Controller
{
    use PrepareResponse;

    /**
     * GET endpoint for /vehicles/{model_year}/{manufacturer}/{model}
     *
     * @param  Nhtsa $nhtsa
     * @param  $model_year
     * @param  $manufacturer
     * @param  $model
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Nhtsa $nhtsa, $model_year, $manufacturer, $model)
    {
        if (!is_numeric($model_year)) {
            return response()
                ->json($this->prepareEmptyResponse());
        }

        $has_rating = ('true' === request()->get('withRating')) ? true : false;

        if ($has_rating) {
            $raw_response = $nhtsa->getVehicleModelsWithRating($model_year, $manufacturer, $model);
        } else {
            $raw_response = $nhtsa->getVehicleModels($model_year, $manufacturer, $model);
        }

        return response()
            ->json($this->prepareResponse($raw_response, $has_rating));
    }
}
