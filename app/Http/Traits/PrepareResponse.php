<?php

namespace App\Http\Traits;

Trait PrepareResponse
{
    /**
     * Prepare an empty response, used in some specific scenarios.
     *
     * @return array
     */
    private function prepareEmptyResponse()
    {
        return [
            'Count' => 0,
            'Results' => [],
        ];
    }

    /**
     * Prepare the response for the endpoint.
     *
     * @param  $raw_data
     * @param  $has_rating
     * @return array
     */
    private function prepareResponse($raw_data, $has_rating = false)
    {
        $results = [];

        foreach ($raw_data['Results'] as $vehicle) {
            $item['Description'] = $vehicle['VehicleDescription'];
            $item['VehicleId'] = $vehicle['VehicleId'];

            if ($has_rating) {
                $item['CrashRating'] = $vehicle['CrashRating'];
            }

            $results[] = $item;
        }

        return [
            'Count' => $raw_data['Count'],
            'Results' => $results,
        ];
    }
}