<?php

namespace App\Models\Services;

use GuzzleHttp;

/**
 * Handle the api calls to the service of New Car Assessment Program: https://one.nhtsa.gov/webapi/Default.aspx
 *
 * Class Nhtsa
 * @package App\Models\Services
 */
class Nhtsa
{
    /**
     * Client to make the api calls.
     *
     * @var GuzzleHttp\Client|null
     */
    private $http_client;

    /**
     * List of endpoints.
     *
     * @var array
     */
    private $end_points = [
        'vehicle_models' => 'https://one.nhtsa.gov/webapi/api/SafetyRatings/modelyear/{model_year}/make/{manufacturer}/model/{model}?format=json',
        'vehicle_rating' => 'https://one.nhtsa.gov/webapi/api/SafetyRatings/VehicleId/{vehicle_id}?format=json',
    ];

    /**
     * Nhtsa constructor.
     *
     * @param null $http_client
     */
    public function __construct($http_client = null)
    {
        if (is_null($http_client)) {
            $http_client = new GuzzleHttp\Client();
        }

        $this->http_client = $http_client;
    }

    /**
     * Get all the vehicle models.
     *
     * @param  $model_year
     * @param  $manufacturer
     * @param  $model
     * @return mixed
     */
    public function getVehicleModels($model_year, $manufacturer, $model)
    {
        $url = strtr($this->end_points['vehicle_models'], [
            '{model_year}' => $model_year,
            '{manufacturer}' => $manufacturer,
            '{model}' => $model,
        ]);

        $response = $this->http_client->get($url);

        $data = $response
            ->getBody()
            ->getContents();

        return json_decode($data, true);
    }

    /**
     * Get all the information of a model vehicle crash test.
     *
     * @param  $vehicle_id
     * @return mixed
     */
    public function getVehicleRating($vehicle_id)
    {
        $url = strtr($this->end_points['vehicle_rating'], [
            '{vehicle_id}' => $vehicle_id,
        ]);

        $response = $this->http_client->get($url);

        $data = $response
            ->getBody()
            ->getContents();

        return json_decode($data, true);
    }

    /**
     * Get all the vehicle models and include the crash test rating.
     *
     * @param  $model_year
     * @param  $manufacturer
     * @param  $model
     * @return mixed
     */
    public function getVehicleModelsWithRating($model_year, $manufacturer, $model)
    {
        $models = $this->getVehicleModels($model_year, $manufacturer, $model);

        foreach($models['Results'] as &$vehicle) {
            $vehicle_data = $this->getVehicleRating($vehicle['VehicleId']);
            $vehicle['CrashRating'] = $vehicle_data['Results'][0]['OverallRating'];
        }

        return $models;
    }
}