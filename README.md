## Stack

* PHP 7.1.9
* Laravel 5.6

## Installation

Clone the repository:

`git clone https://github.com/joseraul/assignment.git`

Open the cloned repository and execute:

`composer install`

To install all the dependencies.

## Execution

Inside the project folder, execute:

`php artisan serve --port=8080`

It will run the Laravel development server and the api will be ready to be used.

## Endpoints
### GET
`GET http://localhost:8080/vehicles/2015/Audi/A3`
### POST
`POST http://localhost:8080/vehicles`
```
{
    "modelYear": 2015,
    "manufacturer": "Audi",
    "model": "A3"
}
```

## Testing
 
 To run the tests execute
 
 `vendor/bin/phpunit`
 
 Inside the project folder. 